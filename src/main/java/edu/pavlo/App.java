package edu.pavlo;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class App {
  public static final String FILE_PATH = "src/main/resources/inputtext.txt";

  //
  // Створити програму опрацювання тексту підручника з програмування з використанням класів: слово,
  // речення, розділовий знак та ін. У всіх задачах з формуванням тексту замінити табуляції і
  // послідовності пробілів одним пробілом.
  //    2.	Вивести всі речення заданого тексту у порядку зростання кількості слів у кожному з них.
  //    14.	У заданому тексті знайти слово паліндромом, тобто, читається зліва на право і справа на
  // ліво однаково.
  public static void main(String[] args) {

    String textForAnalysis = FileReader.readLineByLine(FILE_PATH);

    System.out.println("Input Text");
    System.out.println(textForAnalysis);

    System.out.println("\n\n   Text after removing duplicate white spaces");
    TextAnalyzer textAnalyzer = new TextAnalyzer();
    System.out.println(textAnalyzer.removeDuplicateWhiteSpaces(textForAnalysis));

    List<Sentence> sentences = textAnalyzer.getSentences(textForAnalysis);

    System.out.println("\n\n   Sorted sentences by number of words");
    List<Sentence> sortedSentences =
        sentences.stream()
            .sorted(Comparator.comparing(Sentence::getNumberOfWords))
            .collect(Collectors.toList());
    sortedSentences.forEach(
        sentence ->
            System.out.println(
                sentence.getSentence() + "  wordsNumber = " + sentence.getNumberOfWords()));

    List<Word> words = textAnalyzer.getWords(textForAnalysis);

    words.forEach(
        word -> {
          if (word.isPalindrome()) {
            System.out.println(word.getWord());
          }
        });
  }
}
