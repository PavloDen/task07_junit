package edu.pavlo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextAnalyzer {
  public String removeDuplicateWhiteSpaces(String input) {
    // s means space
    // + means 1 or more
    return input.replaceAll("\\s+", " ");
  }

  public List<Sentence> getSentences(String inText) {
    List<Sentence> sentences = new ArrayList<>();
    // [\\.|!|;] means point or ! or ;
    //  () means group
    //  \\s means space
    //  + means 1 or more
    String[] strings = inText.split("(\\.|!|;)\\s+");
    Arrays.stream(strings).forEach(s -> sentences.add(new Sentence(s)));
    return sentences;
  }

  public List<Word> getWords(String inText) {
    List<Word> words = new ArrayList<>();
    String[] strings = inText.split("\\W+");

    Arrays.stream(strings).forEach(s -> words.add(new Word(s)));
    return words;
  }
}
