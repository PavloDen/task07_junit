package edu.pavlo;

import java.util.Objects;

public class Sentence {
  private String sentence;
  private int numberOfWords;

  public Sentence() {}

  public Sentence(String sentence) {
    this.sentence = sentence;
    this.numberOfWords = sentence.split("\\W+").length;
  }

  public String getSentence() {
    return sentence;
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public int getNumberOfWords() {
    return numberOfWords;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Sentence sentence1 = (Sentence) o;
    return getNumberOfWords() == sentence1.getNumberOfWords() &&
            getSentence().equals(sentence1.getSentence());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getSentence(), getNumberOfWords());
  }
}
