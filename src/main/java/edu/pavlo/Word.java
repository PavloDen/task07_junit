package edu.pavlo;

import java.util.Objects;

public class Word {
  private String word;

  public Word() {}

  public Word(String word) {
    this.word = word;
  }

  public String getWord() {
    return word;
  }

  public boolean isPalindrome() {
    return word.equals(new StringBuilder(word).reverse().toString());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Word word1 = (Word) o;
    return Objects.equals(getWord(), word1.getWord());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getWord());
  }
}
