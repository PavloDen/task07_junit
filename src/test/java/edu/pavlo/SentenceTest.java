package edu.pavlo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SentenceTest {
  private Sentence sentence;

  @Test
  public void getNumberOfWords() {
    String testString = "aa ss ddd. hh l. jjj";
    sentence = new Sentence(testString);
    int expectedWordCount = 6;
    assertEquals(expectedWordCount, sentence.getNumberOfWords());

    String testStringSecond = "aa ss ddd. hh l. jjj, ff! oo!";
    sentence = new Sentence(testStringSecond);
    expectedWordCount = 8;
    assertEquals(expectedWordCount, sentence.getNumberOfWords());
  }

  @Test
  public void getNumberOfWordsNegativeTest() {
    String testString = "aa ss ddd. hh l. jjj";
    Sentence sentence = new Sentence(testString);
    int expectedWordCount = 8;
    assertNotEquals(expectedWordCount, sentence.getNumberOfWords());

    String testStringSecondTest = "aa ss ddd. hh l. jjj, ff! oo!";
    Sentence sentenceSecond = new Sentence(testStringSecondTest);
    expectedWordCount = 9;
    assertNotEquals(expectedWordCount, sentenceSecond.getNumberOfWords());
  }
}
