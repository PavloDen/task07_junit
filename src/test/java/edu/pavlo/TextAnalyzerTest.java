package edu.pavlo;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TextAnalyzerTest {
  private TextAnalyzer textAnalyzer;

  @Before
  public void initTest() {
    textAnalyzer = new TextAnalyzer();
  }

  @Test
  public void removeDuplicateWhiteSpaces() {
    assertEquals("dd dd", textAnalyzer.removeDuplicateWhiteSpaces("dd   dd"));
  }

  @Test
  public void getSentences() {
    String testStr = "Dd dd. Vvv v";
    List<Sentence> actual = textAnalyzer.getSentences(testStr);

    List<Sentence> expected = new ArrayList<>();
    expected.add(new Sentence("Dd dd"));
    expected.add(new Sentence("Vvv v"));

    assertTrue(actual.containsAll(expected));
  }

  @Test
  public void getWords() {
    String testString = "aa ss ddd. hh l. jjj";
    List<Word> actual = textAnalyzer.getWords(testString);

    List<Word> expected = new ArrayList<>();
    expected.add(new Word("aa"));
    expected.add(new Word("aa"));
    expected.add(new Word("ss"));
    expected.add(new Word("ddd"));
    expected.add(new Word("hh"));
    expected.add(new Word("l"));
    expected.add(new Word("jjj"));

    assertTrue(actual.containsAll(expected));
  }
}
