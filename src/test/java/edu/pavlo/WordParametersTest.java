package edu.pavlo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class WordParametersTest {
  private String wordTest;
  boolean testResult;

  public WordParametersTest(String wordTest, boolean testResult) {
    this.wordTest = wordTest;
    this.testResult = testResult;
  }

  @Parameterized.Parameters
  public static Iterable<Object[]> dataForTest() {
    return Arrays.asList(
        new Object[][] {
          {"aaa", true}, // is palindrome true
          {"aaab", false}, // is palindrome false
          {"a", true}, // is palindrome true
        });
  }

  @Test
  public void isPalindromeParamTest() {
    Word word = new Word(wordTest);
    assertEquals(testResult, word.isPalindrome());
  }
}
