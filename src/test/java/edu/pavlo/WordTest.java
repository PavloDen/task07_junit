package edu.pavlo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WordTest {
  private Word word;

  @Test
  public void isPalindromeTest() {
    boolean expected = true;
    word = new Word("abba");
    boolean actual = word.isPalindrome();
    assertEquals(expected, actual);
  }

  @Test
  public void isPalindromeNegativeTest() {
    boolean expected = false;
    word = new Word("abc");
    boolean actual = word.isPalindrome();
    assertEquals(expected, actual);
    assertEquals(expected, new Word("abc abc").isPalindrome());
  }
}
